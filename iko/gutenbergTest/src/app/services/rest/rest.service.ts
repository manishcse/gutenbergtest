import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const options = ({withCredentials: false});

@Injectable({
  providedIn: 'root'
})
export class RestService {

	constructor(private http: HttpClient) {}

	GET(url: string): Observable<any> {
		return this.http.get(url, options)
		.map( (response: any) => {
			return response;
		})
	}

	POST(url: string, data): Observable<any> {
		return this.http.post(url, data, options)
		.map( (response: any) => {
			return response;
		})
	}

	PUT(url: string, data): Observable<any> {
		return this.http.put(url, data, options)
		.map( (response: any) => {
			return response;
		})
	}

	DELETE(url: string): Observable<any> {
		return this.http.delete(url, options)
		.map( (response: any) => {
			return response;
		})
	}
}
