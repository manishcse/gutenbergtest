import { Component, AfterViewInit } from '@angular/core';
import { RestService } from './services/rest/rest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
	title = 'gutenbergTest';
	data = '';
	URL_TO_MATCH = '/sites/default/files/inline-images/';
	REPLACEMENT_URL = `https://stage.api.ikointl.com/sites/default/files/inline-images/`;

	constructor(
		private restService: RestService
	) { }

	ngAfterViewInit(): void {
		this.getPage();
	}

	getPage() {
		const url = 'https://stage.api.ikointl.com/api/v1/page.json';
		this.restService.GET(url)
		.subscribe((result)=>{
			this.data = result[0].body;
			this.data = this.data.replace(new RegExp(this.URL_TO_MATCH, 'g'), this.REPLACEMENT_URL);
			// console.log("ressult: ", this.data);
		}, error => {
			console.log("error : ", error)
		})
	}
}
